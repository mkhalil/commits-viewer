namespace CommitsViewer
{
	public class Application : Gtk.Application
	{
		public Application()
		{
			Object(application_id : "org.gnome.gitlab.mahmoudkhalil.commits-viewer",
				flags: ApplicationFlags.FLAGS_NONE);
		}
		
		protected override void activate()
		{
			var window = new ApplicationWindow(this);
			window.set_title("Open A New Project");
			window.set_default_size(500, 500);
			window.show_all();
		}
	}
}
