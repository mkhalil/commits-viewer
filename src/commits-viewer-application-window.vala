namespace CommitsViewer
{
	[GtkTemplate (ui = "/org/gnome/commits-viewer/ui/commits-viewer-application-window.ui")]
	public class ApplicationWindow : Gtk.ApplicationWindow
	{
		[GtkChild]
		private Gtk.HeaderBar d_header_bar;
		
		[GtkChild]
		private Gtk.Stack d_main_stack;
		
		private MainView d_main_view;
		private CommitsListView d_commits_list_view;
		
		private Ggit.Repository repo;
		
		public ApplicationWindow(Application app)
		{
			Object(application : app);
		}
		
		construct
		{
			print("constructed\n");
			Ggit.init();
			build_ui();
		}
		
		private void build_ui()
		{
			d_main_view = new MainView();
			d_commits_list_view = new CommitsListView();
			
			d_main_stack.add_named(d_main_view, "main_view");
			d_main_stack.add_named(d_commits_list_view, "commits_list_view");

			d_main_stack.set_visible_child_name("main_view");
			
			d_main_view.add_project_button.clicked.connect(() => {
				var file_chooser = new Gtk.FileChooserDialog("Choose Repo", 
				                                             this as Gtk.Window, 
				                                             Gtk.FileChooserAction.SELECT_FOLDER, 
				                                             Gtk.Stock.CANCEL,
				                                             Gtk.ResponseType.CANCEL,
				                                             Gtk.Stock.OPEN,
				                                             Gtk.ResponseType.ACCEPT);
				file_chooser.show();
				
				file_chooser.response.connect(open_response_cb);
			});
		}
		
		public void open_response_cb(Gtk.Dialog dialog, int response_id)
		{
			if(response_id == Gtk.ResponseType.ACCEPT)
			{
				var open_dialog = dialog as Gtk.FileChooserDialog;
				
				var repo_directory = open_dialog.get_file();
				load_project_from_directory(repo_directory);
			}
			
			dialog.destroy();
		}
		
		private void load_project_from_directory(File directory)
		{
			try
			{
				//var location = File.new_for_path(directory);
				repo = Ggit.Repository.open(directory);
				
				print("Opened Project: %s\n", repo.workdir.get_basename());
				
				d_main_stack.set_visible_child_name("commits_list_view");
				d_commits_list_view.load_model(repo);
				d_header_bar.title = repo.workdir.get_basename();
			}
			catch(Error err)
			{
				print(err.message + "\n");
			}
		}
	}
}
